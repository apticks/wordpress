<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'apt' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-T0RSOV+i@_BW/^{M_Sc@1;bKI+md6d~IFQT{/A<eVI9zDnYEYu8*F[+dVm{_(|v' );
define( 'SECURE_AUTH_KEY',  'aDzhS$,* jkdsyig=t~WmREJ.*0iQ,qEW0PSPoAUP=v]u@w*{Dy2z!C$AaOJdM{O' );
define( 'LOGGED_IN_KEY',    'H$nChdv:.=$,`N1P,]{,&fj}JRo1#k|TmIMUOprU>d]F?mO-hZAMO>$a/gN`FYLy' );
define( 'NONCE_KEY',        '<If-Hyh$v#z6=W[`VUsoB*w_q|?8~=S~3_KoGW6IK7kXnNR9~z59N$n;@Kq@k>lr' );
define( 'AUTH_SALT',        'SpR%x}7Q[ALA-At(*Z.o$[5DBsM}@;iAj>$a}Vb)XIy Id`%u35Z(>eN(d P-rc(' );
define( 'SECURE_AUTH_SALT', '@@LnIR;LIW}n:U2<U*++hu|<ZEeB*e9KyRB5^8gdrHbGTQ;ys&Uyr7tQ$GI.:s3E' );
define( 'LOGGED_IN_SALT',   'R:J[3eU0XfY9>!O)f$YRd)f1[[Y;ITV6IW7;[+)9w5Wz>CCjVJ~Zu.6c%7njn;8H' );
define( 'NONCE_SALT',       ';c E_nP|<0Fgdqk|l[]i$m2C`SKIEjCA 0iT7,Kq@,R<Lh.jg.Osb@O_gt]QG`J*' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
