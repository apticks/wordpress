<?php
/**
 * WRC Pricing Tables 2.2.7
 * by @realwebcare - https://www.realwebcare.com/
 *
 * Pricing Table Shortcode
 * Display pricing table responsively.
 * In the tablet view, the pricing table will be divided into two columns.
 * In the mobile view, each column of the price table will be displayed below each other.
**/
function wrcpt_shortcode( $p_table, $tableID, $p_feature, $p_combine, $tot_feat, $p_lists, $p_options, $p_count, $flag ) {
	$i = 1; $j = 0;

	/* Preparing structural settings of the different parts of the table. */
	if(isset($p_combine['cwidth']) && $p_combine['cwidth']) {
		$container_width = (int)$p_combine['cwidth'];
	} else { $container_width = 100; }
	if(isset($p_combine['maxcol']) && $p_combine['maxcol']) {
		$maximum_column = (int)$p_combine['maxcol'];
	} else { $maximum_column = 4; }
	if(isset($p_combine['tbody']) && $p_combine['tbody']) {
		$title_box_height = (int)$p_combine['tbody'];
	} else { $title_box_height = 62; }
	if(isset($p_combine['pbody']) && $p_combine['pbody']) {
		$price_box_height = (int)$p_combine['pbody'];
	} else { $price_box_height = 120; }
	if(isset($p_combine['btbody']) && $p_combine['btbody']) {
		$button_box_height = (int)$p_combine['btbody'];
	} else { $button_box_height = 60; }
	if(isset($p_combine['btbody']) && ($p_combine['bwidth'] && (int)$p_combine['bwidth'] < 120)) {
		$button_padding = (int)$p_combine['bwidth'] * 0.0833;
	} else { $button_padding = 10; }
	if(isset($p_combine['capwidth']) && $p_combine['capwidth']) {
		$caption_width = (int)$p_combine['capwidth'];
	} else { $caption_width = 18.73; }
	if(isset($p_combine['colgap']) && $p_combine['colgap']) {
		$margin = (int)$p_combine['colgap'];
	} else { $margin = 0; }
	if(isset($p_combine['colshad'])) {
		$column_shadow = $p_combine['colshad'];
		$shadow_color = $p_combine['cscolor'];
	} else {
		$column_shadow = 'yes';
		$shadow_color = '#cccccc';
	}

	/* Calculating responsive width of the pricing table for both tablet and mobile view.
	 * Here, we are calculating responsive width both for caption column and package column.
	 */
	if($p_combine) {
		if($p_combine['autocol'] == 'no') {
			if($p_count > $maximum_column) {
				$width = ($container_width)/$maximum_column . '%';
				$cap_width = ($container_width)/($maximum_column+1) . '%';
			} else {
				$width = ($container_width)/$p_count . '%';
				$cap_width = ($container_width)/($p_count+1) . '%';
			}
		} else {
			if($p_combine['ftcap'] == "yes") {
				$single_column_width = ((100 - $caption_width) - ($margin * (12-1)))/12;
			} else {
				$single_column_width = (100 - ($margin * (12-1)))/12;
			}
			$width = ($single_column_width * (12 / $maximum_column)) + ($margin * ((12 / $maximum_column) - 1)) . '%';
			$cap_width = $caption_width . '%';
		}
		$tab_width = (((100 - ($margin * (12-1)))/12) * (12 / 2)) + ($margin * ((12 / 2) - 1)) . '%';
		$mob_width = ((100/12 * (12 / 1))) . '%';
	}

	/* Start making the shortcode */
	if(!empty($p_lists) && $p_combine['enable'] == 'yes' && $flag == 1) { ?>
		<div id="<?php echo $tableID; ?>" class="wrcpt_content wrcpt_container">
			<div class="wrc_pricing_table wrcpt_row">
<style type="text/css">
/* Responsive View */
@media only screen and (max-width : 480px) {div#<?php echo $tableID; ?> div.package_details {width: <?php echo $mob_width; ?>;margin-top: 40px !important}}
@media only screen and (min-width : 481px) and (max-width: 1023px) {div#<?php echo $tableID; ?> div.package_details {width: <?php echo $tab_width; ?>}}
<?php if($p_combine['ftcap'] != "yes") { ?>
<?php if($p_combine['autocol'] == 'yes') { ?>
@media only screen and (min-width : 481px) and (max-width: 1023px) {div#<?php echo $tableID; ?> div.package_details:nth-of-type(2n+1) {margin-right:<?php echo $p_combine['colgap']; ?>%}}
<?php } ?>
@media screen and (min-width: 1024px) {div#<?php echo $tableID; ?> div.package_details {<?php if($p_combine['autocol'] == 'yes') { ?>margin-right:<?php echo $p_combine['colgap']; ?>%;<?php } ?>width: <?php echo $width; ?>}div#<?php echo $tableID; ?> div.package_details:nth-of-type(<?php echo $maximum_column; ?>n+<?php echo $maximum_column; ?>) {margin-right: 0}}
<?php } else { ?>
<?php if($p_combine['autocol'] == 'yes') { ?>
@media only screen and (min-width : 481px) and (max-width: 1023px) {div#<?php echo $tableID; ?> div.package_details:nth-of-type(2n+2) {margin-right:<?php echo $p_combine['colgap']; ?>%}}
<?php } ?>
@media screen and (min-width: 1024px) {
div#<?php echo $tableID; ?> div.package_caption {width: <?php echo $cap_width; ?>}
<?php if($p_combine['ctsize']) { ?>div.wrcpt_content h3.caption {font-size: <?php echo $p_combine['ctsize']; ?>;}<?php } ?>
div#<?php echo $tableID; ?> div.package_details {<?php if($p_combine['autocol'] == 'yes') { ?>width: <?php echo $width; ?>;margin-right:<?php echo $p_combine['colgap']; ?>%;<?php } else { ?>width: <?php echo $cap_width; ?>;<?php } ?>}
div#<?php echo $tableID; ?> div.package_details:nth-of-type(<?php echo $maximum_column + 1; ?>n+<?php echo $maximum_column + 1; ?>) {margin-right: 0}
<?php if($p_combine['tbody']) { ?>div#<?php echo $tableID; ?> div.package_caption li.pricing_table_title {height: <?php echo $p_combine['tbody']; ?>;}<?php } ?>
<?php if($p_combine['pbody']) { ?>div#<?php echo $tableID; ?> div.package_caption li.pricing_table_plan {height: <?php echo $p_combine['pbody']; ?>;line-height: <?php echo $p_combine['pbody']; ?>;}<?php } ?>
div#<?php echo $tableID; ?> div.package_caption li.feature_style_2 span.caption, div#<?php echo $tableID; ?> div.package_caption li.feature_style_3 span.caption, div#<?php echo $tableID; ?> div.package_caption li.feature_style_2 span.cap_tooltip, div#<?php echo $tableID; ?> div.package_caption li.feature_style_3 span.cap_tooltip, div#<?php echo $tableID; ?> div.package_details li.feature_style_1 span {<?php if($p_combine['cftsize']) { ?>font-size: <?php echo $p_combine['cftsize']; ?>;<?php } ?>}
<?php if($p_combine['ftbody']) { ?>div#<?php echo $tableID; ?> div.package_caption li.feature_style_2, div#<?php echo $tableID; ?> div.package_caption li.feature_style_3 {height: <?php echo $p_combine['ftbody']; ?>;line-height: <?php echo $p_combine['ftbody']; ?>;}<?php } ?>
}
<?php } ?>
/* End of responsive view */
<?php if($p_combine['cwidth']) { ?>div#<?php echo $tableID; ?> {width:<?php echo $container_width; ?>%}<?php } ?>
<?php if($column_shadow == 'yes') { ?>
div#<?php echo $tableID; ?> div.package_details {-moz-box-shadow: 0px 0px 3px <?php echo $shadow_color; ?>;-webkit-box-shadow: 0px 0px 3px <?php echo $shadow_color; ?>;filter: progid:DXImageTransform.Microsoft.Shadow(Strength=0, Direction=0, Color='<?php echo $shadow_color; ?>');-ms-filter: progid:DXImageTransform.Microsoft.Shadow(Strength=0, Direction=0, Color='<?php echo $shadow_color; ?>');box-shadow: 0px 0px 3px <?php echo $shadow_color; ?>}
div#<?php echo $tableID; ?> div.package_details:hover {-moz-box-shadow: 0px 0px 3px <?php echo $p_combine['cshcolor']; ?>;-webkit-box-shadow: 0px 0px 3px <?php echo $p_combine['cshcolor']; ?>;filter: progid:DXImageTransform.Microsoft.Shadow(Strength=0, Direction=0, Color='<?php echo $p_combine['cshcolor']; ?>');-ms-filter: progid:DXImageTransform.Microsoft.Shadow(Strength=0, Direction=0, Color='<?php echo $p_combine['cshcolor']; ?>');box-shadow: 0px 0px 3px <?php echo $p_combine['cshcolor']; ?>}
<?php } ?>
<?php if($p_combine['encol'] != "yes") { ?>div#<?php echo $tableID; ?> div.package_details:hover {-moz-transform: none;-webkit-transform: none;-o-transform: none;-ms-transform: none;transform: none}<?php } ?>
<?php if($p_combine['dscol'] == "yes") { ?>div#<?php echo $tableID; ?> div.package_details:hover {box-shadow: none;}<?php } ?>
<?php if($p_combine['ttwidth']) { ?>div#<?php echo $tableID; ?> div.package_caption span.cap_tooltip:hover:after, div#<?php echo $tableID; ?> div.package_details span.text_tooltip:hover:after, div#<?php echo $tableID; ?> div.package_details span.icon_tooltip:hover:after {width: <?php echo $p_combine['ttwidth']; ?>}<?php } ?>
div#<?php echo $tableID; ?> div.package_caption li.feature_style_2, div#<?php echo $tableID; ?> div.package_caption li.feature_style_3 {<?php if($p_combine['entips'] == 'yes') { ?>padding-right: 25px;<?php } ?>}
</style><?php
				foreach($p_options as $key => $value) {
					$packageType = get_option($value);
					if(isset($packageType['pdisp'])) { $prdisp = $packageType['pdisp']; }
					else { $prdisp = 'show'; }
					if(($j < 1 || $j % $maximum_column == 0) && $j <= $p_count) {
						$i = 1;
						if($p_combine['ftcap'] == "yes" && $prdisp == "show") { ?>
							<div class="package_caption">
								<ul>
									<li class="pricing_table_title"></li>
									<li class="pricing_table_plan">
										<h3 class="caption"><?php echo ucwords(str_replace('_', ' ', $p_table)); ?></h3>
									</li><?php
									for($tf = 1; $tf <= $tot_feat; $tf++) {
										if($i % 2 == 0) {
											if($i == $tot_feat) { ?>
												<li class="feature_style_2 bottom_left"><div class="caption_lists"><span class="caption"><?php echo $p_feature['fitem'.$tf]; ?></span></div></li><?php
											} else { ?>
												<li class="feature_style_2"><div class="caption_lists"><span class="caption"><?php echo $p_feature['fitem'.$tf]; ?></span></div></li><?php
											}
										} else {
											if($i == 1) { ?>
												<li class="feature_style_3 top_left"><div class="caption_lists"><span class="caption"><?php echo $p_feature['fitem'.$tf]; ?></span></div></li><?php
											} elseif($i == $tot_feat) { ?>
												<li class="feature_style_3 bottom_left"><div class="caption_lists"><span class="caption"><?php echo $p_feature['fitem'.$tf]; ?></span></div></li><?php
											} else { ?>
												<li class="feature_style_3"><div class="caption_lists"><span class="caption"><?php echo $p_feature['fitem'.$tf]; ?></span></div></li><?php
											}
										} $i++;
									} ?>
								</ul>
							</div><?php
						}
					}
					$tlight = adjustBrightness($packageType['tbcolor'], 50);
					$p_color = $packageType['tbcolor'];
					$pdark = adjustBrightness($packageType['tbcolor'], 20);
					$blight = adjustBrightness($packageType['bcolor'], 50);
					$bdark = adjustBrightness($packageType['bhover'], 20);
					$bhlight = adjustBrightness($packageType['bhover'], 50);
					$rlight = adjustBrightness($packageType['rbcolor'], 80);
					$rdark = adjustBrightness($packageType['rbcolor'], 20);
					$i = 1; ?>
<style type="text/css">
<?php if($p_combine['tsize'] || $packageType['tcolor']) { ?>
div#<?php echo $tableID; ?> div.package_details h3.txcolor-<?php echo $packageType['pid']; ?> {<?php if($p_combine['tsize']) { ?>font-size: <?php echo $p_combine['tsize']; ?>;<?php } ?><?php if($packageType['tcolor']) { ?>color: <?php echo $packageType['tcolor']; ?>;<?php } ?>}
<?php } ?>
div#<?php echo $tableID; ?> div.package_details li.color-<?php echo $packageType['pid']; ?> {background: -moz-linear-gradient(<?php echo $tlight; ?>, <?php echo $packageType['tbcolor']; ?>);background: -webkit-gradient(linear, center top, center bottom, from(<?php echo $tlight; ?>), to(<?php echo $packageType['tbcolor']; ?>));background: -webkit-linear-gradient(<?php echo $tlight; ?>, <?php echo $packageType['tbcolor']; ?>);background: -o-linear-gradient(<?php echo $tlight; ?>, <?php echo $packageType['tbcolor']; ?>);background: -ms-linear-gradient(<?php echo $tlight; ?>, <?php echo $packageType['tbcolor']; ?>);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='<?php echo $tlight; ?>', endColorstr='<?php echo $packageType['tbcolor']; ?>',GradientType=1);-ms-filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='<?php echo $tlight; ?>', endColorstr='<?php echo $packageType['tbcolor']; ?>',GradientType=1);background: linear-gradient(<?php echo $tlight; ?>, <?php echo $packageType['tbcolor']; ?>);<?php if($p_combine['tbody']) { ?>height: <?php echo $p_combine['tbody']; ?>;line-height: <?php echo $p_combine['tbody']; ?>;<?php } ?>}
<?php if($p_combine['psbig'] || $packageType['pcbig']) { ?>
div#<?php echo $tableID; ?> div.package_details h2.txcolor-<?php echo $packageType['pid']; ?> {<?php if($p_combine['psbig']) { ?>font-size: <?php echo $p_combine['psbig']; ?>;<?php } ?><?php if($packageType['pcbig']) { ?>color: <?php echo $packageType['pcbig']; ?>;<?php } ?>}div#<?php echo $tableID; ?> div.package_details h2.txcolor-<?php echo $packageType['pid']; ?> span.price {<?php if($p_combine['psbig']) { ?>font-size: <?php echo $p_combine['psbig']; ?>;<?php } ?><?php if($packageType['pcbig']) { ?>color: <?php echo $packageType['pcbig']; ?>;<?php } ?>}
<?php } ?>
<?php if($p_combine['pssmall'] || $packageType['pcbig']) { ?>
div#<?php echo $tableID; ?> div.package_details h2.txcolor-<?php echo $packageType['pid']; ?> span.unit, div#<?php echo $tableID; ?> div.package_details h2.txcolor-<?php echo $packageType['pid']; ?> span.cent {<?php if($p_combine['pssmall']) { ?>font-size: <?php echo $p_combine['pssmall']; ?>;<?php } ?><?php if($packageType['pcbig']) { ?>color: <?php echo $packageType['pcbig']; ?>;<?php } ?>}
<?php } ?>
<?php if($p_combine['purgt'] == 'yes') { ?>div#<?php echo $tableID; ?> div.package_details h2.txcolor-<?php echo $packageType['pid']; ?> span.unit{position: absolute;right: 20px}<?php } ?>
<?php if($p_combine['pbody']) { ?>div#<?php echo $tableID; ?> div.package_details li.plan-<?php echo $packageType['pid']; ?> {height: <?php echo $p_combine['pbody']; ?>;line-height: <?php echo $p_combine['pbody']; ?>}<?php } ?>
div#<?php echo $tableID; ?> div.package_details li.plan-<?php echo $packageType['pid']; ?>, div#<?php echo $tableID; ?> div.package_details li.bbcolor-<?php echo $packageType['pid']; ?> {background: -moz-linear-gradient(<?php echo $pdark; ?>, <?php echo $p_color; ?>);background: -webkit-gradient(linear, center top, center bottom, from(<?php echo $pdark; ?>), to(<?php echo $p_color; ?>));background: -webkit-linear-gradient(<?php echo $pdark; ?>, <?php echo $p_color; ?>);background: -o-linear-gradient(<?php echo $pdark; ?>, <?php echo $p_color; ?>);background: -ms-linear-gradient(<?php echo $pdark; ?>, <?php echo $p_color; ?>);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $pdark; ?>', endColorstr='<?php echo $p_color; ?>',GradientType=1 );-ms-filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $pdark; ?>', endColorstr='<?php echo $p_color; ?>',GradientType=1 );background: linear-gradient(<?php echo $pdark; ?>, <?php echo $p_color; ?>);}
<?php if($p_combine['btbody']) { ?>div#<?php echo $tableID; ?> div.package_details li.bbcolor-<?php echo $packageType['pid']; ?> {height:<?php echo $p_combine['btbody']; ?>;line-height: 100%}<?php } ?>
div#<?php echo $tableID; ?> div.package_details li.button-<?php echo $packageType['pid']; ?> .action_button {background: -moz-linear-gradient(<?php echo $blight; ?>, <?php echo $packageType['bcolor']; ?>);background: -webkit-gradient(linear, center top, center bottom, from(<?php echo $blight; ?>), to(<?php echo $packageType['bcolor']; ?>));background: -webkit-linear-gradient(<?php echo $blight; ?>, <?php echo $packageType['bcolor']; ?>);background: -o-linear-gradient(<?php echo $blight; ?>, <?php echo $packageType['bcolor']; ?>);background: -ms-linear-gradient(<?php echo $blight; ?>, <?php echo $packageType['bcolor']; ?>);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $blight; ?>', endColorstr='<?php echo $packageType['bcolor']; ?>',GradientType=1 );-ms-filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $blight; ?>', endColorstr='<?php echo $packageType['bcolor']; ?>',GradientType=1 );background: linear-gradient(<?php echo $blight; ?>, <?php echo $packageType['bcolor']; ?>);border:1px solid <?php echo $packageType['bcolor']; ?>;<?php if($p_combine['btsize']) { ?>font-size: <?php echo $p_combine['btsize']; ?>;<?php } ?><?php if($packageType['btcolor']) { ?>color: <?php echo $packageType['btcolor']; ?>;<?php } ?><?php if($p_combine['bheight']) { ?>height:<?php echo $p_combine['bheight']; ?>;line-height:<?php echo $p_combine['bheight']; ?>;<?php } ?><?php if($p_combine['bwidth']) { ?>width:<?php echo $p_combine['bwidth']; ?>;padding:0 <?php echo round($button_padding); ?>px<?php } ?>}
div#<?php echo $tableID; ?> div.package_details li.button-<?php echo $packageType['pid']; ?> .action_button:hover {background: -moz-linear-gradient(<?php echo $bhlight; ?>, <?php echo $packageType['bhover']; ?>);background: -webkit-gradient(linear, center top, center bottom, from(<?php echo $bhlight; ?>), to(<?php echo $packageType['bhover']; ?>));background: -webkit-linear-gradient(<?php echo $bhlight; ?>, <?php echo $packageType['bhover']; ?>);background: -o-linear-gradient(<?php echo $bhlight; ?>, <?php echo $packageType['bhover']; ?>);background: -ms-linear-gradient(<?php echo $bhlight; ?>, <?php echo $packageType['bhover']; ?>);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $bhlight; ?>', endColorstr='<?php echo $packageType['bhover']; ?>',GradientType=1 );-ms-filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $bhlight; ?>', endColorstr='<?php echo $packageType['bhover']; ?>',GradientType=1 );background: linear-gradient(<?php echo $bhlight; ?>, <?php echo $packageType['bhover']; ?>);border:1px solid <?php echo $packageType['bhover']; ?>;color: <?php echo $packageType['bthover']; ?>;}
div#<?php echo $tableID; ?> div.package_details li.button-<?php echo $packageType['pid']; ?> .action_button span {<?php if($packageType['btcolor']) { ?>color: <?php echo $packageType['btcolor']; ?>;<?php } ?>}

div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?>, div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> a.wrc_tooltip, div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> div.feat_cap, div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> span.text_tooltip, div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> span.icon_tooltip, div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> span.fa-icon, div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> span.hide-fa-icon {<?php if($p_combine['ftsize']) { ?>font-size: <?php echo $p_combine['ftsize']; ?>;<?php } ?>}
div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> span.feat_value, div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> span.media_screen {<?php if($p_combine['cftsize']) { ?>font-size: <?php echo $p_combine['cftsize']; ?>;<?php } ?>}
<?php if($p_combine['ftbody']) { ?>div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> {height: <?php echo $p_combine['ftbody']; ?>;line-height: <?php echo $p_combine['ftbody']; ?>}<?php } ?>
<?php if($p_combine['ftcap'] == "yes") { ?>div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> {padding:0 5px 0 20px}<?php }?>
div#<?php echo $tableID; ?> div.package_details li.ftcolor-<?php echo $packageType['pid']; ?> {text-align:<?php echo $p_combine['ftdir']; ?>}
<?php if($p_combine['enribs'] == 'yes') { ?>
div#<?php echo $tableID; ?> div.package_details li .ribbon_color-<?php echo $packageType['pid']; ?> {top:<?php echo $title_box_height + 1; ?>px}
<?php if($p_combine['rtsize']) { ?>div#<?php echo $tableID; ?> div.package_details li .ribbon_color-<?php echo $packageType['pid']; ?> {font-size: <?php echo $p_combine['rtsize']; ?>}<?php } ?>
div#<?php echo $tableID; ?> div.package_details li .ribbon_color-<?php echo $packageType['pid']; ?> a {background: <?php echo $packageType['rbcolor']; ?>;background: -moz-linear-gradient(left, <?php echo $rlight; ?>, <?php echo $packageType['rbcolor']; ?>);background: -webkit-linear-gradient(left, <?php echo $rlight; ?>, <?php echo $packageType['rbcolor']; ?>);background: -o-linear-gradient(left, <?php echo $rlight; ?>, <?php echo $packageType['rbcolor']; ?>);background: -ms-linear-gradient(left, <?php echo $rlight; ?>, <?php echo $packageType['rbcolor']; ?>);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $rlight; ?>', endColorstr='<?php echo $packageType['rbcolor']; ?>',GradientType=1 );-ms-filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='<?php echo $rlight; ?>', endColorstr='<?php echo $packageType['rbcolor']; ?>',GradientType=1);background: linear-gradient(to right, <?php echo $rlight; ?>, <?php echo $packageType['rbcolor']; ?>);color: <?php echo $packageType['rtcolor']; ?>}
div#<?php echo $tableID; ?> div.package_details li .ribbon_color-<?php echo $packageType['pid']; ?> a:before {border-color: transparent <?php echo $rlight; ?> transparent transparent}
div#<?php echo $tableID; ?> div.package_details li .ribbon_color-<?php echo $packageType['pid']; ?> a:after {border-top: 8px solid rgba(0,0,0,.5)}
<?php } ?>
<?php if($packageType['spack'] == 'yes') {
	$column_number = $packageType['pid']; ?>
	div#<?php echo $tableID; ?> div.special-package li.plan-<?php echo $packageType['pid']; ?> {height: <?php echo $price_box_height+20; ?>px !important;line-height: <?php echo $price_box_height+20; ?>px !important}
	div#<?php echo $tableID; ?> div.special-package li.pricing_table_button {height:<?php echo $button_box_height+10; ?>px !important;line-height:<?php echo $button_box_height-3; ?>px !important;padding-top: 5px}
	div#<?php echo $tableID; ?> div.special-package {z-index: 100;margin-top: 0;}
	div#<?php echo $tableID; ?> div.special-package li.pricing_table_title {height: <?php echo $title_box_height+20; ?>px !important;line-height: <?php echo $title_box_height+20; ?>px !important}
	<?php if($p_combine['btbody'] == '') { ?>
	div#<?php echo $tableID; ?> div.special-package li.pricing_table_button {height:80px !important;line-height:80px !important}
	<?php } else { ?>
	div#<?php echo $tableID; ?> div.special-package li.pricing_table_button {height:<?php echo (int)$p_combine['btbody']+20; ?>px !important;line-height:<?php echo (int)$p_combine['btbody']+20; ?>px !important}
	<?php } ?>
	<?php if($p_combine['enribs'] == 'yes') { ?>
		div#<?php echo $tableID; ?> div.special-package li .wrc-ribbon {top:<?php echo $title_box_height + 21; ?>px}
	<?php } ?>
<?php } ?>
</style>
					<?php if($prdisp == 'show') { ?>
					<div class="package_details<?php echo ' package-'.$packageType['pid']; ?><?php if(isset($packageType['spack']) && $packageType['spack'] == 'yes') { echo ' special-package'; } ?>">
						<ul>
							<?php if($p_combine['enribs'] == "yes" && $packageType['rtext'] != '') { ?><li><div class="wrc-ribbon ribbon_color-<?php echo $packageType['pid']; ?>"><a href="#" id="wrc-ribbon"><?php echo $packageType['rtext']; ?></a></div></li><?php } ?>
                            <li class="pricing_table_title color-<?php echo $packageType['pid']; ?> title_top_radius">
                                <h3 class="package_type txcolor-<?php echo $packageType['pid']; ?>"><?php echo $packageType['type']; ?></h3>
                            </li>
                            <li class="pricing_table_plan plan-<?php echo $packageType['pid']; ?> top_price">
                                <h2 class="package_plan txcolor-<?php echo $packageType['pid']; ?>">
                                    <span class="unit"><?php echo $packageType['unit']; ?></span><span class="price"><?php echo $packageType['price']; ?></span><span class="cent"><?php if($packageType['cent']) echo '.'.$packageType['cent']; ?></span><span class="plan"><?php echo $packageType['plan']; ?></span>
                                </h2>
                            </li><?php
							if($p_feature) {
								for($tf = 1; $tf <= $tot_feat; $tf++) {
									if(isset($packageType['fitem'.$tf])) {
										if($p_feature['ftype'.$tf] != 'textcheck') {
											$f_value = $packageType['fitem'.$tf];
										} else {
											$f_value = $packageType['fitem'.$tf];
											$tc_value = $packageType['fitem'.$tf.'c'];
										}
										$f_tips = $packageType['tip'.$tf];
									}
									if ($i % 2 == 0) { $row_color = 'rowcolor'; } else { $row_color = 'altrowcolor'; } ?>
                                    <li class="feature_style_1 ftcolor-<?php echo $packageType['pid']; ?> <?php echo $row_color; ?><?php if($tf == 1) { echo ' top-feature'; } ?><?php if($tf == $tot_feat) { echo ' last-feature'; } ?>">
                                        <div class="feature_lists"><?php
                                            if($f_value == 'tick' || $f_value == 'cross') {
                                                if($p_combine['ftcap'] == "yes") {
                                                    $ttip = 'icon';
                                                    if($f_value == 'tick') { ?><span class="feature_yes"></span><?php
                                                    } else { ?><span class="feature_no"></span><?php } ?>
                                                    <span class="media_screen"><?php echo $p_feature['fitem'.$i]; ?></span><?php
                                                } else {
                                                    $ttip = 'text';
                                                    if($f_value == 'tick') { ?><span class="feature_yes"></span><?php
                                                    } else { ?><span class="feature_no"></span><?php } ?>
                                                    <span class="feat_value"><?php echo $p_feature['fitem'.$i]; ?></span><?php
                                                }
                                            } elseif($f_value == '') {
                                                if($p_combine['ftcap'] == "yes") {
                                                    $ttip = 'icon'; ?>
                                                    <span class="media_screen<?php if($f_value == '' && $tc_value != 'tick') { ?> not-available<?php } ?>"><?php echo $p_feature['fitem'.$i]; ?></span><?php
                                                } else {
                                                    $ttip = 'text'; echo $fa_icon; ?>
                                                    <span class="feat_value<?php if($f_value == '' && $tc_value != 'tick') { ?> not-available<?php } ?>"><?php echo $p_feature['fitem'.$i]; ?></span><?php
                                                }
                                            } else {
                                                $ttip = 'text';
                                                if($p_combine['ftcap'] == "yes") { ?>
                                                    <div class="feat_cap"><?php echo $f_value; ?></div>
                                                    <span class="media_screen"><?php echo $p_feature['fitem'.$i]; ?></span><?php
                                                } else { ?>
                                                    <div class="feat_cap"><?php echo $f_value; ?></div>
                                                    <span class="feat_value"><?php echo $p_feature['fitem'.$i]; ?></span><?php
                                                }
                                            }
                                            if($p_combine['entips'] == "yes" && $f_tips != '') { ?><span class="<?php echo $ttip; ?>_tooltip" rel="<?php echo $f_tips; ?>"></span><?php } ?>
                                        </div>
                                    </li><?php
                                    $i++;
								} $j++;
							} ?>
							<li class="pricing_table_button bbcolor-<?php echo $packageType['pid']; ?> button-<?php echo $packageType['pid']; ?>">
								<div class="button_code">
									<a href="<?php echo $packageType['blink']; ?>" class="action_button"<?php if($p_combine['nltab'] == 'yes') { ?> target="_blank"<?php } ?>><?php echo $packageType['btext']; ?></a>
								</div>
							</li>
						</ul>
					</div> <!-- End of package_details -->
					<?php } ?>
				<?php } ?> <!-- End of ForEach -->
			</div>
		</div>
		<div class="wrc_clear"></div>
	<?php } else { ?>
		<style type="text/css">
			p.wrcpt_notice {background-color: #FFF;padding: 15px 20px;font-size: 24px;line-height: 24px;border-left: 4px solid #7ad03a;-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);box-shadow: 2px 2px 5px 2px rgba(0,0,0,.1);display: inline-block}
		</style><?php
		if($p_table == '' || $flag == 0) { ?>
			<p class="wrcpt_notice"><?php echo __('You didn\'t add any pricing tables yet!', 'wrcpt') ?></p><?php
		} elseif(empty($p_lists)) { ?>
			<p class="wrcpt_notice"><?php echo __('You didn\'t add any pricing column yet!', 'wrcpt') ?></p><?php
		} else { ?>
			<p class="wrcpt_notice"><?php echo __('Please <strong>Enable</strong> pricing table to display pricing table columns!', 'wrcpt') ?></p><?php
		}
	}
}
?>