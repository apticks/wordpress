<?php
/*
 * WRC Pricing Tables 2.2.7
 * @realwebcare - https://www.realwebcare.com/
 * Generating preview of the pricing table in WP admin panel
 * to get an idea about how the table will look at front-end
 */
function wrcpt_view_pricing_packages() {
	$i = 1;
	$pricing_table = $_POST['packtable'];
	$tableId = $_POST['tableid'];
	$package_lists = get_option($pricing_table);
	$packageOptions = explode(', ', $package_lists);
	$packageCount = count($packageOptions); ?>
	<style type="text/css">
		*,*:after,*:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0;margin:0}
	</style>
	<div id="tabledisplaydiv">
		<h3><span id="editPackages" class="button button-large" onclick="wrcpteditpackages(<?php echo $packageCount; ?>, '<?php echo $pricing_table; ?>')"><?php _e('Edit Columns', 'wrcpt'); ?></span></h3>
		<?php echo do_shortcode('[wrc-pricing-table id="'.$tableId.'"]'); ?>
	</div>
<?php
	die;
}
add_action( 'wp_ajax_nopriv_wrcpt_view_pricing_packages', 'wrcpt_view_pricing_packages' );
add_action( 'wp_ajax_wrcpt_view_pricing_packages', 'wrcpt_view_pricing_packages' );
?>