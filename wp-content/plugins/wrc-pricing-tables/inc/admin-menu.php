<?php
/*
 * WRC Pricing Tables 2.2.7
 * @realwebcare - https://www.realwebcare.com/
 * Adding menu for Pricing Table in WP admin
 */
add_action('admin_menu', 'wrcpt_register_menu');
define( 'WRCPT_DOMAIN', 'wrcpt' );
function wrcpt_register_menu() {
	add_menu_page('WRC Pricing Table', __('Pricing Tables', WRCPT_DOMAIN ), 'add_users', __FILE__, 'wrcpt_plugin_menu', plugins_url( '../images/icon.png', __FILE__ ));
	add_submenu_page(__FILE__, __('WRCPT Lists', WRCPT_DOMAIN ), __('All Pricing Tables', WRCPT_DOMAIN ), 'add_users', __FILE__, 'wrcpt_plugin_menu');
	add_submenu_page(__FILE__, 'WRCPT Template', __('Templates', WRCPT_DOMAIN ), 'add_users', 'wrcpt_template', 'wrcpt_template_page');
	add_submenu_page(__FILE__, 'WRCPT Guide', __('Guide', WRCPT_DOMAIN ), 'add_users', 'wrcpt_guide', 'wrcpt_guide_page');
}
function wrcpt_guide_page() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include ( WRCPT_PLUGIN_PATH . 'inc/wrcpt-guide.php' );
}
function wrcpt_template_page() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.', WRCPT_DOMAIN ) );
	}
	include ( WRCPT_PLUGIN_PATH . 'template/process-template.php' );
}
function wrcpt_plugin_menu() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.', WRCPT_DOMAIN ) );
	}
	include ( WRCPT_PLUGIN_PATH . 'inc/process-table.php' );
}
include ( WRCPT_PLUGIN_PATH . 'lib/process_table-option.php' );
include ( WRCPT_PLUGIN_PATH . 'lib/process-template-option.php' );
include ( WRCPT_PLUGIN_PATH . 'inc/add-package.php' );
include ( WRCPT_PLUGIN_PATH . 'inc/modify-package.php' );
include ( WRCPT_PLUGIN_PATH . 'inc/process-feature.php' );
include ( WRCPT_PLUGIN_PATH . 'inc/display-package.php' );
include ( WRCPT_PLUGIN_PATH . 'inc/wrcpt-sidebar.php' );
if(isset($_POST['new_pricing_table']) && $_POST['new_pricing_table'] == "newtable") {
	if( isset( $_POST['wrcpt_add_new'] ) ) { wrcpt_add_new_table(); }
}
?>