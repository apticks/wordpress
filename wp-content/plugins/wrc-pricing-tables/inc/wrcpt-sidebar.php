<?php
/*
 * WRC Pricing Tables 2.2.7
 * @realwebcare - https://www.realwebcare.com/
 * Plugin info in sidebar
 */
 function wrcpt_sidebar() { ?>
	<div class="postbox-container" style="width:25%">
		<div id="sidebar">
			<div id="wrcusage-info" class="wrcusage-sidebar">
				<h3>Plugin Info</h3>
				<ul class="wrcusage-list">
					<li>Version: <a href="https://wordpress.org/plugins/wrc-pricing-tables/changelog/" target="_blank">2.2.7</a></li>
					<li>Requires: Wordpress 3.5+</li>
					<li>First release: 6 May, 2015</li>
					<li>Last Update: 18 October, 2019</li>
					<li>Developed by: <a href="https://www.realwebcare.com" target="_blank">Realwebcare</a></li>
					<li>Need Help? <a href="https://wordpress.org/support/plugin/wrc-pricing-tables/" target="_blank">Support</a></li>
                    <li>Benefited by WRC Pricing Tables? Please leave us a <a target="_blank" href="https://wordpress.org/support/plugin/wrc-pricing-tables/reviews/?filter=5/#new-post">&#9733;&#9733;&#9733;&#9733;&#9733;</a> rating. We highly appreciate your support!</li>
					<li>Published under:<br>
					<a href="http://www.gnu.org/licenses/gpl.txt" target="_blank">GNU General Public License</a>
                    </li>
				</ul>
			</div>
			<div id="wrcusage-features" class="wrcusage-sidebar">
				<h3>Premium Features</h3>
				<div class="ccwrpt">Premium version has been developed to present Pricing Tables more proficiently. Some of the most notable features are:</div>
				<ul class="wrcusage-list">
					<li>25 to 50+ templates to create table instantly.</li>
					<li>Import/Export (Backup) pricing tables.</li>
					<li>Make a copy of a table instantly.</li>
					<li>Hide empty features.</li>
					<li>Hide any parts of the pricing table.</li>
					<li>Set price section at the bottom.</li>
					<li>PayPal button integration.</li>
					<li>Image and video support.</li>
					<li>Pricing toggle support.</li>
				</ul>
				<a href="https://code.realwebcare.com/item/clean-css3-wordpress-responsive-pricing-table/" target="_blank">View Premium</a>
			</div>
		</div>
	</div>
<?php } ?>