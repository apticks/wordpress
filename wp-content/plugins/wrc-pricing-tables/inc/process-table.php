<?php
/*
 * WRC Pricing Tables 2.2.7
 * @realwebcare - https://www.realwebcare.com/
 * Adding a new Pricing Table
 * Lists of all created pricing tables
 */
$package_table = get_option('packageTables');
$package_ids = get_option('packageIDs');
$url = htmlspecialchars($_SERVER['HTTP_REFERER']);
?>
<div class="wrap">
	<div id="add_new_table" class="postbox-container" style="width:75%;">
	<h2 class="main-header"><?php _e('Pricing Tables', 'wrcpt'); ?> <a href="#" id="new_table" class="add-new-h2"><?php _e('Add New', 'wrcpt'); ?></a><span id="wrcpt-loading-image"></span></h2>
	<form id='wrcpt_new' method="post" action="">
		<input type="hidden" name="new_pricing_table" value="newtable" />
		<div id="tablenamediv">
			<div class="tablenamewrap">
				<h3><?php _e('Pricing Table Name', 'wrcpt'); ?></h3>
				<input type="text" name="pricing_table" size="30" value="" id="title" autocomplete="off" placeholder="<?php _e('Enter Pricing Table Name','wrcpt'); ?>" required />
			</div>
			<div id="pricingfeaturediv">
				<div class="pricingfeaturewrap">
					<h3><?php _e('Pricing Package Features', 'wrcpt'); ?></h3>
					<table id="feature_additem" cellspacing="0">
						<tr class="featheader">
							<th><?php _e('Features', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter your pricing table features in the text box. A feature is a distinctive characteristic of a good or service that sets it apart from similar items. Means of providing benefits to customers.', 'wrcpt'); ?>"></a></th>
							<th><?php _e('Type', 'wrcpt'); ?></th>
							<th><?php _e('Actions', 'wrcpt'); ?></th>
						</tr>
						<tr class="featurebody">
							<td><input type="text" name="feature_name[]" value="" placeholder="<?php _e('Enter Feature Name', 'wrcpt'); ?>" size="20" required /></td>
							<td>
								<select name="feature_type[]" id="feature_type">
									<option value="text" selected="selected"><?php _e('Text', 'wrcpt'); ?></option>
									<option value="check"><?php _e('Checkbox', 'wrcpt'); ?></option>
								</select>
							</td>
							<td><span id="remDisable"></span></td>
						</tr>
					</table>
					<input type="button" id="addfeature" class="button-primary" value="<?php _e('Add New', 'wrcpt'); ?>" />
				</div>
			</div>
			<input type="submit" id="wrcpt_add_new" name="wrcpt_add_new" class="button-primary" value="<?php _e('Add Table', 'wrcpt'); ?>" />
		</div>
	</form>
	<?php if($package_table) { ?>
		<div class="table_list">
			<form id='wrcpt_edit_form' method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="wrcpt_edit_process" value="editprocess" />
				<table id="wrcpt_list" class="form-table">
					<thead>
						<tr>
							<th><?php _e('ID', 'wrcpt'); ?></th>
							<th><?php _e('Table Name', 'wrcpt'); ?></th>
							<th><?php _e('Shortcode', 'wrcpt'); ?></th>
							<th><?php _e('Template', 'wrcpt'); ?></th>
							<th><?php _e('Visible', 'wrcpt'); ?></th>
						</tr>
					</thead>
			<?php
			$table_lists = explode(', ', $package_table);
			$id_lists = explode(', ', $package_ids);
			foreach($table_lists as $key => $list) {
				$list_item = ucwords(str_replace('_', ' ', $list));
				$package_lists = get_option($list);
				$package_feature = get_option($list.'_feature');
				$packageCombine = get_option($list.'_option');
				$package_item = explode(', ', $package_lists);
				$packageCount = count($package_item);
				$packageID = $id_lists[$key];
				$tableId = $key+1; $t_templ = 'temp0'; ?>
				<?php if($package_feature) { ?>
					<?php if(get_option($list) && $packageCount > 0) { ?>
						<tbody id="wrcpt_<?php echo $list; ?>">
							<tr <?php if($tableId % 2 == 0) { echo 'class="alt"'; } ?>>
								<td><?php echo $tableId; ?></td>
								<td class="table_name" id="<?php echo $list; ?>">
									<div onclick="wrcpteditpackages(<?php echo $packageCount; ?>, '<?php echo $list; ?>')"><?php echo $list_item; ?></div>
									<span id="edit_package" onclick="wrcpteditpackages(<?php echo $packageCount; ?>, '<?php echo $list; ?>')"><?php _e('Edit Columns', 'wrcpt'); ?></span>
									<span id="add_feature" onclick="wrcpteditfeature('<?php echo $list; ?>')"><?php _e('Edit Features', 'wrcpt'); ?></span>
									<span id="view_package" onclick="wrcptviewpack(<?php echo $packageID; ?>, '<?php echo $list; ?>')"><?php _e('Preview', 'wrcpt'); ?></span>
									<span id="remTable" onclick="wrcptdeletetable('<?php echo $list; ?>')"><?php _e('Delete', 'wrcpt'); ?></span>
								</td>
								<td>
                                    <div class="tooltip">
                                		<input id="myInput-<?php echo $tableId; ?>" type="text" name="wrc_shortcode" class="wrc_shortcode" value="<?php echo esc_html('[wrc-pricing-table id="'.$packageID.'"]'); ?>" onclick="myFunction(<?php echo $tableId; ?>)" onmouseout="outFunc()">
                                        <span class="tooltiptext" id="myTooltip-<?php echo $tableId; ?>">Click to Copy Shortcode!</span>
                                    </div>
                                </td>
								<td><div class="temp_choice">
								<?php if(isset($packageCombine['templ']) && $packageCombine['templ']!=''){$t_templ=$packageCombine['templ'];} ?>
                                	<select name="wrcpt_template" id="wrcpt_template" onChange="wrcpttemplate('<?php echo $list; ?>', this.options[this.selectedIndex].value)">
                                    	<option value="temp0">Default</option>
                                    	<option value="temp1"<?php if($t_templ == 'temp1') { ?> selected<?php } ?>>Template 1</option>
                                    	<option value="temp2"<?php if($t_templ == 'temp2') { ?> selected<?php } ?>>Template 2</option>
                                    </select>
                                </div></td>
								<td><?php if($packageCombine['enable'] == 'yes') {_e('Yes', 'wrcpt');} else {_e('No', 'wrcpt');} ?></td>
							</tr>
						</tbody>
					<?php } else { ?>
						<tbody id="wrcpt_<?php echo $list; ?>">
							<tr <?php if($tableId % 2 == 0) { echo 'class="alt"'; } ?>>
								<td><?php echo $tableId; ?></td>
								<td class="table_name" id="<?php echo $list; ?>">
									<div onclick="wrcptaddpack('<?php echo $list; ?>')"><?php echo $list_item; ?></div>
									<span id="add_package" onclick="wrcptaddpack('<?php echo $list; ?>')"><?php _e('Add Columns', 'wrcpt'); ?></span>
									<span id="add_feature" onclick="wrcpteditfeature('<?php echo $list; ?>')"><?php _e('Edit Features', 'wrcpt'); ?></span>
									<span id="remTable" onclick="wrcptdeletetable('<?php echo $list; ?>')"><?php _e('Delete', 'wrcpt'); ?></span>
								</td>
								<td class="wrcpt_notice"><span><?php _e('Mouseover on the table name in the left and clicked on <strong>Add Columns</strong> link. After adding pricing columns you will get the <strong>SHORTCODE</strong> here.', 'wrcpt'); ?></span></td>
								<td><?php _e('Not Ready!', 'wrcpt'); ?></td>
								<td><?php _e('No', 'wrcpt'); ?></td>
							</tr>
						</tbody>
					<?php } ?>
				<?php } else { ?>
					<tbody id="wrcpt_<?php echo $list; ?>">
						<tr <?php if($tableId % 2 == 0) { echo 'class="alt"'; } ?>>
							<td><?php echo $tableId; ?></td>
							<td class="table_name" id="<?php echo $list; ?>">
								<div onclick="wrcpteditfeature('<?php echo $list; ?>')"><?php echo $list_item; ?></div>
								<span id="add_feature" onclick="wrcpteditfeature('<?php echo $list; ?>')"><?php _e('Add Features', 'wrcpt'); ?></span>
								<span id="remTable" onclick="wrcptdeletetable('<?php echo $list; ?>')"><?php _e('Delete', 'wrcpt'); ?></span>
							</td>
							<td class="wrcpt_notice"><span><?php _e('Mouseover on the table name in the left and clicked on <strong>Add Feature</strong> link. To get started you have to add some pricing features first. After that, you will be able to add pricing columns. After adding pricing columns you will get the <strong>SHORTCODE</strong> here.', 'wrcpt'); ?></span></td>
							<td><?php _e('Not Ready!', 'wrcpt'); ?></td>
							<td><?php _e('No', 'wrcpt'); ?></td>
						</tr>
					</tbody>
				<?php } ?>
			<?php } ?>
				</table><br>
                <input type="button" id="reset-shortcode" name="reset_shortcode" class="button-primary" onclick="wrcptresetshortcode()" value="<?php _e('Regenerate Shortcode', 'wrcpt'); ?>" />
			</form>
		</div>
	<?php } else { ?>
		<div class="table_list">
			<p class="get_started"><?php _e('You didn\'t add any pricing tables yet! Click on <strong>Add New</strong> button to get started. If you feel trouble to understand how to create pricing table, then navigate to <strong>Pricing Tables >> Template</strong>. There you will get 3 ready-made pricing table templates. Simply choose one template and click on <strong>Create Table</strong> button to create a pricing table instantly!<br /><br />If you feel trouble to understand the plugin, navigate to <strong>Pricing Tables >> Guide</strong> and follow the guidelines described there.', 'wrcpt'); ?></p>
		</div>
	<?php } ?>
	</div><!-- End postbox-container -->
	<?php wrcpt_sidebar(); ?>
</div>